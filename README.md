# README

## Synopsis
-----------------

A structured, carefully architectured MVC application on MERN stack.

- ### Profile Display

    This project aims to create a user profile, wherein the user fills in necessary details via various forms. Then the filled in details are shown to do the user in an editable page, where he/she can view and edit the details if required.

    ##### Routes

    > Profile Display (and edit): http://localhost:8081/ProfileDisplay

    > Personal Info form        : http://localhost:8081/personalinfo

    > Experience Info Form      : http://localhost:8081/Experience


- ### Sample Components

    Sample components have been created on top of semantic-react for the purpose, keeping in mind the component way of working of React, for the purpose of reusability

    ##### Routes

    > Sample Components: http://localhost:8081/Examples


## File Structure
-----------------

### React

* **components** : contains all elemental components to be reused

* **collections**: contains composite components consuming components

    * **Forms**: All the info forms to be filled by the user. these are elemental forms [Profile Display]

    * **composite**
        * **FormRender** : Composite forms which consumes elemental forms [Profile Display]
        * **Display** :    Various composite components which will finally be rendered on the views [Profile Display]

    * **examples** :   sample components are consumed in this composite component which is finally rendered on sample components view [Sample components]



## How to run
-----------------

After cloning the project run

```sh

$  npm install
$  gulp server

```


## Integrations
-----------------

  > Semantic React:  [https://github.com/hallister/semantic-react](https://github.com/hallister/semantic-react)

  >  Docs         :  [http://hallister.github.io/semantic-react/#Form](http://hallister.github.io/semantic-react/#Form)
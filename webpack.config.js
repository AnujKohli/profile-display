module.exports = {

	entry: {
	    home              : './app/collections/home.js',  // can be given more in array ['./public/js/app.js', './public/js/main.js']
		index             : './app/collections/examples/index.js',
		ProfileDisplay    : './app/collections/composite/Display/ProfileDisplay.js',
		PersonalDisplay   : './app/collections/composite/Display/PersonalDisplay.js',
		EducationDisplay  : './app/collections/composite/Display/EducationDisplay.js',
		ExperienceDisplay : './app/collections/composite/Display/ExperienceDisplay.js',
		AddressDisplay    : './app/collections/composite/Display/AddressDisplay.js',
		FormsFillUp       : './app/collections/composite/Display/FormsFillUp.js'
	},
	output: {
	    filename: 'bundle-[name].js',
	    path: __dirname + '/built'
	},

	module: {
		// Pre loader JSHint indentifies possible js errors
		preLoaders: [
	      {
	        test: /\.js$/,
	        exclude: /node_modules/,
	        loader: 'jshint-loader'

	      }
	   ],

		// loader babel check for es6 syntax
	   	loaders: [
	    {
	       test: [/\.js$/, /\.es6$/],
	       exclude: /node_modules/,
	       loader: 'babel-loader',
	       query: {
	         presets: ['react', 'es2015']
	       }
	    }
	   ]
	 },
	 resolve: {
	   extensions: ['', '.js', '.es6']
	 },
	 
  watch: true
}

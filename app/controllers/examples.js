
module.exports = function(){

    var controller = {};

    controller.index = function(req, res){
        res.render('examples/index', { message : "Hello World !"});
    };

    controller.navbar = function(req, res){
        res.render('examples/navbar', { message : "Hello World !"});
    };

    return controller;
}
import React from 'react';

/**
 * Commmon is a directory which will contain components which might act as sub-components
 */


/**
 * NavBar component
 */
class NavBar extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className ="remove">
                {
                    this.props.navlinks.map(function(name)
                        {
                            return <NavLink name = {name} key={name}/>;
                        }
                    )
                }
            </div>
        );
    }
}

/**
 * Navlink component. sub-component of NavBar
 */
class NavLink extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {

        return (
            <a className="item"> {this.props.name}</a>
        );
    }
}

export default NavBar;

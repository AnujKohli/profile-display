
import React from 'react';
import DatePickerUI from 'react-datepicker';
import moment from 'moment';



export default class DatePicker extends React.Component {

    constructor(props) {
        super(props);
        this.state= {
            startDate: moment()
        }
    }

    handleChange(date) {
        this.setState ({
            startDate: date
        });
    }

        render() {
        return (

             <DatePickerUI
                selected={this.state.startDate}
                showYearDropdown = {true}
                showMonthDropdown = {true}
                disabled = {this.props.disabled}
                onChange={this.handleChange.bind(this)} />

        );
    }
}
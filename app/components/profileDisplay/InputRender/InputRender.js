
import React from 'react';
import { Input, Grid, Column, Span, SubHeader } from 'semantic-react';


class InputRender extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            value: this.props.value
        };
    }

    onInputChange(event){
        this.setState({
            value: event.target.value
        });
    }

    render() {
        return (


                        <Input          value={this.state.value}
                                        onChange={this.onInputChange.bind(this)}
                                        placeholder={this.props.placeholder}
                                        fluid={this.props.fluid}
                                        type={this.props.type}
                                        name={this.props.name}
                                        disabled={this.props.disabled}

                        />





        );
    }
}


InputRender.defaultProps = {
    placeholder:        "",
    fluid:              "",
    type:               "text",
    value:              "",
    label : ""
};


export default InputRender;
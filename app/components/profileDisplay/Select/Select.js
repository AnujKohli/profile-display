
import { Select as SelectReactUi, Option } from 'semantic-react';
import React from 'react';
import ReactDom from 'react-dom';

export default class Select extends React.Component {

    constructor(props) {
        super(props);
        this.state= {
            active: false,
            values: [],
            search: '',
            options: []
        };
    }

    render() {
        this.state.options=this.props.options;
        return (

            <SelectReactUi active={this.state.active}
                    search
                    selection
                    allowAdditions
                    placeholder={this.props.placeholder}
                    selected={this.state.values}
                    onSelectChange={val => {
                        this.setState({
                            values: val,
                            options: [...new Set(this.state.options.concat(val))],
                            active: false
                        })
                    }}
                    onClick={() => this.setState({active: true})}
                    onRequestClose={() => this.setState({active: false})}
                    onSearchStringChange={search => this.setState({search: search})}
                    searchString={this.state.search}
            >
                {this.state.options.map(o => <Option value={o} key={o}>{o}</Option>)}

            </SelectReactUi>

        );
    }
}

Select.defaultProps = {
    options:     ['10th', '12th'],
    placeholder: 'Select type'
};

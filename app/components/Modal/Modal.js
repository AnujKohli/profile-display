/**
 *
 *
 *  [] Usage =>  <DynamicModal open={this.state.open} />
 *  [] Where {this.state.open} is a state of Component needs to open Modal
 *
 *
  TODO: Modal components need to be made dynamic through props
 *
 */


import React from 'react';
import { Row } from  'semantic-react';
import { Grid } from  'semantic-react';
import { Column } from 'semantic-react';
import { Button } from 'semantic-react';
import { Modal as ModalReactUI, Icon, Header, Content, Image, Description, Actions, LabeledButton } from 'semantic-react';


class Modal extends React.Component {

    constructor(props, defaultProps) {
        super(props, defaultProps);

        this.state = {
            active: false
        }

    }

    onCloseModal() {
        this.setState({ active: false, event: "no" });
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.open) {
            this.setState({ active: true });
        }
    }

    render() {

        return (

                <div>
                    <ModalReactUI onRequestClose={ this.onCloseModal.bind(this) } active={ this.state.active }>
                        <Icon name="close" onClick={ this.onCloseModal.bind(this) }/>
                        <Header>{this.props.modal_header}</Header>
                        <Content image>
                            <Image size={this.props.image_size} wrapComponent src={this.props.image_src} />
                            <Description>
                                <Header>{this.props.description_header}</Header>
                                {this.props.description}
                            </Description>
                        </Content>
                        <Actions>
                            <Button emphasis="negative" onClick={ this.onCloseModal.bind(this) }>{this.props.close_btn_title}</Button>
                            <LabeledButton emphasis="positive"
                                           label="checkmark"
                                           labelType="icon"
                                           onClick={ this.onCloseModal.bind(this) }
                            >
                                {this.props.submit_btn_title}
                            </LabeledButton>
                        </Actions>
                    </ModalReactUI>
                </div>

        );
    }

}

export default Modal;

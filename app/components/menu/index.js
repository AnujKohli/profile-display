
/**
 * Each of our component should have an index page which exports all the classes of our component module
 *
 * This is how we will export from our component module. One of the component class
 * should be made default. This is a practice we would follow.If we have more than one component classes, we would import each as
 * import {abc} from './abc'; and add it in export object.
 *
 */

import {default as MenuDefault} from './MenuDefault';
export { MenuDefault };

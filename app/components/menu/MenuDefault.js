/**
 *
 * JS file containing code of default menu to be rendered
 *
 */

import React from 'react';
import ReactDom from 'react-dom';

//to import navbar to be displayed in the menu
import NavBar from '../common/NavBar';

export default class MenuDefault extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (

                        <div className="ui container">
                            <div className="ui large secondary inverted pointing menu">
                                 <a className="toc item">
                                     <i className="sidebar icon"></i>
                                 </a>
                                <NavBar navlinks = {this.props.navlink} />
                                    <div className="right item">
                                        {/*todo need to create new components for login and signup*/}
                                        <a className="ui inverted button">Log in</a>
                                        <a className="ui inverted button">Sign Up</a>
                                     </div>
                            </div>
                        </div>
        );
    }
}

//ReactDom.render(<MenuDefault />, document.getElementById('menu'));
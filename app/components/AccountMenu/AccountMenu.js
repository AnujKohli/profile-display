
import React from 'react'
import { Dropdown, Image } from 'semantic-ui-react'

class  OptionsCustom extends React.Component{
    constructor(props, defaultProps) {
        super(props, defaultProps);
    }

    render(){
        var i=0;
        return (
            <div>
                <Dropdown trigger={this.props.trigger} pointing='top left' icon={null}>
                    <Dropdown.Menu>
                        {
                            this.props.dropdownItem.map((dropdownItem) => {
                                i++;
                                return <Dropdown.Item key={i} text={dropdownItem.text} icon={dropdownItem.icon}></Dropdown.Item>;
                            })
                        }
                    </Dropdown.Menu>
                </Dropdown>
            </div>
        );
    }
}

export default OptionsCustom
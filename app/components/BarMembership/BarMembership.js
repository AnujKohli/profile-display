
import React from 'react';
import { Icon, Input } from 'semantic-react';

class BarMembership extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            states:     [],
            numbers:    []
        }
    }

    onClicka(){
        var arr=this.state.states;
        var arr1=this.state.numbers;
        arr.push("");
        arr1.push("");
        this.setState({
            states:     arr,
            numbers:    arr1
        });
    }

    onClickm(index){
        var arr=this.state.states;
        var arr1=this.state.numbers;
        var newarr=[];var newarr1=[];
        for(var i=0;i<arr.length;i++){
            if(i!=index) {
                newarr.push(arr[i]);
                newarr1.push(arr1[i]);
            }
        }
        this.setState({
            states:     newarr,
            numbers:    newarr1
        });
    }

    onInputChanges(index, event){
        var arr=this.state.states;
        arr[index]=event.target.value;
        this.setState({
            states: arr
        });
    }

    onInputChangen(index, event){
        var arr=this.state.numbers;
        arr[index]=event.target.value;
        this.setState({
            numbers: arr
        });
    }

    render() {
        var i=0;
        var tbr=[];
        for(i;i<this.state.states.length;i++){
                tbr.push(<div key={i}>State:<Input value={this.state.states[i]} onChange={this.onInputChanges.bind(this, i)} />  Number:<Input value={this.state.numbers[i]} onChange={this.onInputChangen.bind(this, i)} type="number" /> <Icon name="minus square" onClick={this.onClickm.bind(this, i)} /></div>);
        }
        return (
            <div>
                Add Bar Memberships <Icon name="add square" onClick={this.onClicka.bind(this)} />
                {tbr}
            </div>
        );
    }
}

export default BarMembership;
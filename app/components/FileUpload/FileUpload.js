
import React from 'react';
import Dropzone from 'react-dropzone';
import { Icon, Image } from 'semantic-react';


class FileUpload extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            files:  []
        }
    }

    onDrop(files){
        var arrayvar=this.state.files;
        arrayvar.push(files);
        this.setState({
            files:  arrayvar
        })
    }

    onClick(name){
        var arrayVar=this.state.files;
        var newArray=[];
        arrayVar.map((file) => {
            if(file[0].name!=name){
                newArray.push(file);
            }
        });
        this.setState({
            files:  newArray
        })
    }

    render() {
        const liStyle = {
            backgroundColor:    '#FFF',
        };

        return (
            <div>
                <Dropzone onDrop={this.onDrop.bind(this)} />
                {<ul>
                    {
                        this.state.files.map((file) => {
                            return <li style={liStyle} key={file[0].name}> {file[0].name} <Icon name="close" onClick={this.onClick.bind(this, file[0].name)} /> <Image size="large" src={file[0].preview} /> </li>
                        })
                    }
                </ul>}
            </div>
        );
    }
}

export default FileUpload;
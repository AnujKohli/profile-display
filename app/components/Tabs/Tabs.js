
import React from 'react';
import { Tabs as TabsReactUI, TabMenu, Tab, MenuItem } from 'semantic-react';

class Tabs extends React.Component {

    constructor(props, defaultProps) {
        super(props, defaultProps);
        this.state = {
            active: 1
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.open) {
        }
    }

    render() {

        var menu_items = [];
        var tabs = [];
        var i=0, j=0;


        return (
                <TabsReactUI onTabChange={(val) => this.setState({ active: val })} activeTab={this.state.active}>
                    <TabMenu>
                        {
                            this.props.menuTitles.map((menuTitle) => {
                                i++;
                                return <MenuItem key={i} menuValue={i}>{menuTitle}</MenuItem>;
                            })
                        }
                    </TabMenu>

                    {
                        this.props.tabsContent.map((tabContent) => {
                            j++;
                            return <Tab key={j} value={j}>{tabContent}</Tab>;
                        })
                    }
                </TabsReactUI>

        );
    }

}

export default Tabs;


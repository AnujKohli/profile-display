
import React from 'react';
import { Row } from  'semantic-react';
import { Grid } from  'semantic-react';
import { Column } from 'semantic-react';
import { Search } from 'semantic-react';

class ProjectSearch extends React.Component {
    constructor(props) {
        super(props);
        this.state = {value:''}
    }

    render() {
        console.log(this.state.value);
        return (

            <div>
                <Search
                    onChange={e => setState({value: e.currentTarget.value})}
                    onSearchClick={(e, currentValue) => setState({value: currentValue})}
                    value={state.value}
                    placeholder="Search Project"
                    results={[
                        'Alberta',
                        'British Columbia',
                        'Manitoba',
                        'New Brunswick',
                        'Newfoundland and Labrador',
                        'Nova Scotia',
                        'Ontario',
                        'Prince Edward Island',
                        'Quebec',
                        'Saskatchewan',
                        'Northwest Territories',
                        'Nunavut',
                        'Yukon'
                    ]}
                />
            </div>

        );
    }

}

export default ProjectSearch;
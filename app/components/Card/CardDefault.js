

import React from 'react';
import ReactDom from 'react-dom';
import { Card as CardReactUi, Icon, Image } from 'semantic-ui-react'

export default class CardDefault extends React.Component {

    constructor(props) {
        super(props);
        this.state= {

             };
    }

    render() {
        return (

            <CardReactUi.Group>
                {
                    this.props.content.map(eachContent =>

                        <CardReactUi key={eachContent.id}>
                            <Image src={eachContent.img}/>
                            <CardReactUi.Content>
                                <CardReactUi.Header>
                                    {eachContent.header}
                                </CardReactUi.Header>
                                <CardReactUi.Meta>
                            <span className='date'>
                                {eachContent.meta}
                             </span>
                                </CardReactUi.Meta>
                                <CardReactUi.Description>
                                    {eachContent.description}
                                </CardReactUi.Description>
                            </CardReactUi.Content>
                            <CardReactUi.Content extra>
                                <a>
                                    <Icon name='user'/>
                                    {eachContent.extra}
                                </a>
                            </CardReactUi.Content>
                        </CardReactUi>)
                }
                </CardReactUi.Group>

        );
    }
}



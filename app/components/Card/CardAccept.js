
import React from 'react';
import ReactDom from 'react-dom';
import { Card as CardReactUi, Icon, Button, Image } from 'semantic-ui-react'

export default class CardAccept extends React.Component {

    constructor(props) {
        super(props);
        this.state= {

             };
    }

    render() {
        return (

            <CardReactUi.Group>
                {
                    this.props.content.map(eachContent =>

                        <CardReactUi key={eachContent.id}>
                            <CardReactUi.Content>
                                <Image floated='right' size='mini' src={eachContent.img} />
                                <CardReactUi.Header>
                                    {eachContent.header}
                                </CardReactUi.Header>
                                <CardReactUi.Meta>
                                    {eachContent.meta}
                                </CardReactUi.Meta>
                                <CardReactUi.Description>
                                    {eachContent.description}
                                </CardReactUi.Description>
                            </CardReactUi.Content>
                            <CardReactUi.Content extra>
                                <div className='ui two buttons'>
                                    <Button basic color='green'>Approve</Button>
                                    <Button basic color='red'>Decline</Button>
                                </div>
                            </CardReactUi.Content>
                        </CardReactUi>)
                }
                </CardReactUi.Group>

        );
    }
}


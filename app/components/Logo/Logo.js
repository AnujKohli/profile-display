
import React from 'react';
import { Image } from 'semantic-react';

/**
 *
 *  [] Usage =>  <Logo sw="style" />
 *  [] Where style is either gray, white or colour
 */
class Logo extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        var imageurl="/images/LexInsight_logo_colored.png";
        switch(this.props.sw){
            case "colour"   : imageurl = "/images/LexInsight_logo_colored.png";break;
            case "white"    : imageurl = "/images/LexInsight_logo_white.png";break;
            case "gray"     : imageurl = "/images/LexInsight_logo_gray.png";break;
        }
        return (
            <div>
                <a href="http://localhost:8080/">
                    <Image src={imageurl} />
                </a>
            </div>

        );
    }
}

export default Logo;
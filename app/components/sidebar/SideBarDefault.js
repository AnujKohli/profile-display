/**
 *
 * Page for navbar sample
 *
 */

import React from 'react';
import ReactDom from 'react-dom';
import { Row } from  'semantic-react';
import { Grid } from  'semantic-react';
import { Column } from 'semantic-react';
import { Button } from 'semantic-ui-react';

// import custom component
//import SampleForm from './all-samples/SampleForm';

import NavBar from '../common/NavBar';

export default class SideBarDefault extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="ui vertical inverted sidebar menu">
                 <NavBar navlinks = {this.props.navlink} />
            </div>
        );
    }
}


//ReactDom.render(<SideBarDefault />, document.getElementById('navbar'));


import { Items as ItemsReactUi, Item, Header, Meta, Description, Text, Image } from 'semantic-react';
import React from 'react';
import ReactDom from 'react-dom';

export default class Items extends React.Component {

    constructor(props) {
        super(props);
        this.state= {
            content: props.content

        }
    }

        render() {
        return (

            <ItemsReactUi>
                {this.state.content.map(eachContent =>
                    <Item image={eachContent.img} key={eachContent.id}>
                        <Header>{eachContent.header}</Header>
                        <Meta>{eachContent.meta}</Meta>
                        <Description><Image src={eachContent.description} /></Description>
                        <Text extra>{eachContent.text}</Text>
                    </Item>)
                }
            </ItemsReactUi>

        );
    }
}
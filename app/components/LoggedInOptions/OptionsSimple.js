import React from 'react';
import { Row } from  'semantic-react';
import { Grid } from  'semantic-react';
import { Column } from 'semantic-react';
import { Search } from 'semantic-react';
import { Icon, Header, Content, Image, Description, Actions, LabeledButton, DropdownMenu, MenuItem } from 'semantic-react';

class OptionsSimple extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            active: false,
            menuValue: null
        }
    }
    onMenuChange(value) {
        this.setState({
            menuValue: value
        });
    }
    onDropdownClick() {
        this.setState({
            active: !this.state.active
        });
    }
    closeDropdown() {
        this.setState({
            active: false
        });
    }
    render() {
        return (
            <div>
                <DropdownMenu active={this.state.active}
                             label="Simple Dropdown Menu"
                             menuValue={this.state.menuValue}
                             onClick={this.onDropdownClick.bind(this)}
                             onMenuItemClick={this.closeDropdown.bind(this)}
                             onMenuChange={this.onMenuChange.bind(this)}
                             onRequestClose={this.closeDropdown.bind(this)}
               >
                    <MenuItem menuValue={1}>First item</MenuItem>
                    <MenuItem menuValue={2}>Second item</MenuItem>
                    <MenuItem menuValue={3}>Third item</MenuItem>
               </DropdownMenu>
               <h3>You selected: {this.state.menuValue}</h3>
            </div>
        );
    }
}
export default OptionsSimple;
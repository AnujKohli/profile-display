/**
 *
 * To add/remove steps edit the following variables:
 * steps                add another object into the array with its description, title
 * stepContent          add the content of that particular step
 *
 */

import React from 'react';
import { Step } from 'semantic-ui-react';
import { InputRender } from './../InputRender';
import { Select } from './../Select';

class PostProject extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            active : 0
        }
    }

    handleClick(index){
        this.setState({
            active:  index
        });
        console.log(this.state.active);
    }

    render() {
        const { Content, Description, Group, Title } = Step;
        var steps = [
            { title: 'Post Project Description', description: 'Enter the details of the project' },
            { title: 'Attorney Requirements', description: 'Enter the details of what is required of the attorneys' },
        ];
        steps[this.state.active].active=true;
        var i;
        for(i=0;i<steps.length;i++){
            steps[i].onClick=this.handleClick.bind(this, i);
        }
        var stepContent=[];
        stepContent[0]=(<InputRender />);
        stepContent[1]=(<Select />);
        return (
            <div>
                <Group ordered items={steps} />
                <br />
                {stepContent[this.state.active]}
            </div>
        );
    }
}

export default PostProject;
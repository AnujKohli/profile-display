
import { Rating as RatingReactUi} from 'semantic-react';
import React from 'react';
import ReactDom from 'react-dom';

export default class Rating extends React.Component {

    constructor(props) {
        super(props);
        this.state= {
            value: 0
        }
    }

    render() {
        return (

                 <RatingReactUi type="star" value={this.state.value} max={7} onChange={(val) => this.setState({ value: val })}/>

        );
    }
}

import React from 'react'
import { Button, Comment, Form, Header } from 'semantic-ui-react'

class Comments extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        var i=0;
        return (
            <div>
                <Comment.Group>
                    <Header as='h3' dividing>Comments</Header>
                    {
                        this.props.commentItem.map((commentItem) => {
                            i++;
                            return (
                                <Comment key={i}>
                                    <Comment.Avatar src={commentItem.avatarSrc} />
                                    <Comment.Content>
                                        <Comment.Author as='a'>{commentItem.author}</Comment.Author>
                                        <Comment.Metadata>
                                            <div>{commentItem.date}</div>
                                        </Comment.Metadata>
                                        <Comment.Text>{commentItem.text}</Comment.Text>
                                        <Comment.Actions>
                                            <Comment.Action>Reply</Comment.Action>
                                        </Comment.Actions>
                                    </Comment.Content>
                                </Comment>
                            )
                        })
                    }
                    
                    <Form reply onSubmit={e => e.preventDefault()}>
                        <Form.TextArea />
                        <Button content='Add Reply' labelPosition='left' icon='edit' primary />
                    </Form>
                </Comment.Group>
            </div>
        )
    }
}
export default Comments

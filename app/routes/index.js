
module.exports = function(app){
	app.route('/home')
		.get(app.controllers.home.home);

	// All example routes
	app.route('/examples/')
		.get(app.controllers.examples.index);

	app.route('/PersonalInfo/')
		.get(app.controllers.PersonalInfo.index);

	app.route('/Experience/')
		.get(app.controllers.Experience.index);

	app.route('/ProfileDisplay/')
		.get(app.controllers.ProfileDisplay.index);

	app.route('/FormsFillUp/')
		.get(app.controllers.FormsFillUp.index);
}
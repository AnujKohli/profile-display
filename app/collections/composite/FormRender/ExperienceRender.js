
import React from 'react';
import ReactDom from 'react-dom';
import {Field, label, Form, Button, Icon} from 'semantic-react'
import { default as Experience} from '../../Forms/Experience';

/*
 |--------------------------------------------------------------------------
 | Experience Render Form
 |--------------------------------------------------------------------------
 |
 |Composite component to be rendered to fill experience details
 |
 */

export default class ExperienceRender extends React.Component {
    constructor(props) {
        super(props);
        this.onClickMinus = this.onClickMinus.bind(this);

        var companyList    = [];
        var experienceList = [];
        var i = 0;

        this.props.content.map( (eachContent) => {
                companyList.push(eachContent.company),
                    experienceList.push(eachContent.experience),
                    i = i + 1
            }
        );

        if (this.props.disabled) {
              var iconState = 'disabled'
        }
        this.state={
            disabled: this.props.disabled,
            iconState  : iconState,
            label   : "edit",
            index   : i,
            company   : companyList,
            experience: experienceList
        };
    }

    onClick() {
        this.setState ( {
            disabled:!this.state.disabled,
            label   : "save"
        })
        console.log(this.state.disabled)
    }

    onClickAdd(e) {
        e.preventDefault();
        this.setState ( {
            index : this.state.index + 1
        })
    }

    onClickMinus() {
        this.setState ( {
            index : this.state.index - 1
        })
    }

    render() {
        var buttonDisplay;
        if (this.props.fill)
        {
            buttonDisplay = <Button  onClick = {this.props.onClickSave}> Save & Continue </Button>

        }
        else
        {
            buttonDisplay = <Button  onClick = {this.onClick.bind(this)}> { (this.state.disabled)? 'edit': 'save'}  </Button>
        }

        console.log(this.state.company[0]);
        console.log(this.state.index);
        var i=0;
        var fields=[];
        for(i;i<this.state.index;i++){
            fields.push(<Experience
                         disabled = {this.state.disabled}
                         onClickMinus = {this.onClickMinus}
                         company =  {this.state.company[i]}
                         experience =  {this.state.experience[i]}
                         iconState = {this.state.iconState}
                    />);
        }

        return (

           <Form >


                <Button disabled = {this.state.disabled}  onClick = {this.onClickAdd.bind(this) } > Add </Button>
                {fields}
                {buttonDisplay}

            </Form>
        );
    }
}

ExperienceRender.defaultProps = {
    disabled : true,

    content:    [
        {
            company: '',
            experience: ''
        }

    ]

}




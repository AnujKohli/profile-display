
import React from 'react';
import ReactDom from 'react-dom';
import {Field, label, Form, Button, SubHeader, Grid, Column} from 'semantic-react'
import { default as AddressForm} from '../../Forms/Address';

/*
 |--------------------------------------------------------------------------
 | Address Render Form
 |--------------------------------------------------------------------------
 |
 |Composite component to be rendered to fill address details
 |
 */

export default class AddressRender extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            disabled: this.props.disabled,
            label   : "edit"

        };
    }

    onClick() {
        this.setState ( {
            disabled:!this.state.disabled,
            label   : "save"
        })
        console.log(this.state.disabled)
    }

    render() {
        var buttonDisplay;
        if (this.props.fill)
        {
            buttonDisplay = <Button  onClick = {this.props.onClickSave}> Save & Continue </Button>

        }
        else
        {
            buttonDisplay = <Button  onClick = {this.onClick.bind(this)}> { (this.state.disabled)? 'edit': 'save'}  </Button>
        }
        return (
            <Form >
                <Grid>
                    <Column width={6} >
                <Field inline>
                    <SubHeader size="big" disabled color="black">Present Address</SubHeader>
                    <AddressForm disabled = {this.state.disabled}/>
                </Field>
                        </Column>
                    <Column width={6} >
                <Field inline>
                    <SubHeader size="big" disabled color="black">Permanent Address</SubHeader>
                    <AddressForm disabled = {this.state.disabled}/>
                </Field>
                    </Column>
                </Grid>
                <Grid>
                    <Column></Column>
                </Grid>
                {buttonDisplay}
            </Form>
        );
    }
}

AddressRender.defaultProps = {
    disabled : true
}

//ReactDom.render(<AddressRender />, document.getElementById('AddressRender'));


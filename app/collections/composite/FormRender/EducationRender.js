
import React from 'react';
import ReactDom from 'react-dom';
import {Field, label, Form, Button, Icon} from 'semantic-react'
import { default as Education} from '../../Forms/Education';

/*
 |--------------------------------------------------------------------------
 | Education Render Form
 |--------------------------------------------------------------------------
 |
 |Composite component to be rendered to fill education details
 |
 */
export default class EducationRender extends React.Component {
    constructor(props) {
        super(props);
        this.onClickMinus = this.onClickMinus.bind(this);

        var examList = [];
        var boardList = [];
        var percentageList = []
        var i = 0;

        this.props.content.map( (eachContent) => {
            examList.push(eachContent.examType),
                boardList.push(eachContent.board),
                percentageList.push(eachContent.percentage),
                    i = i + 1
            }
        );

        if (this.props.disabled) {
              var iconState = 'disabled'
        }
        this.state={
            disabled: this.props.disabled,
            iconState  : iconState,
            label   : "edit",
            index   : i,
            examType   : examList,
            board: boardList,
            percentage: percentageList
        };
    }

    onClick() {
        this.setState ( {
            disabled:!this.state.disabled,
            label   : "save"
        })
        console.log(this.state.disabled)
    }

    onClickAdd(e) {
        e.preventDefault();
        this.setState ( {
            index : this.state.index + 1
        })
    }

    onClickMinus() {
        this.setState ( {
            index : this.state.index - 1
        })
    }

    render() {

        var buttonDisplay;
        if (this.props.fill)
        {
            buttonDisplay = <Button  onClick = {this.props.onClickSave}> Save & Continue </Button>

        }
        else
        {
            buttonDisplay = <Button  onClick = {this.onClick.bind(this)}> { (this.state.disabled)? 'edit': 'save'}  </Button>
        }

        console.log(this.state.index);
        var i=0;
        var fields=[];
        for(i;i<this.state.index;i++){
            fields.push(<Education
                         disabled = {this.state.disabled}
                         onClickMinus = {this.onClickMinus}
                         examType =  {this.state.examType[i]}
                         board =  {this.state.board[i]}
                         percentage =  {this.state.percentage[i]}
                         iconState = {this.state.iconState}
                    />);
        }

        return (

           <Form >


                <Button disabled = {this.state.disabled}  onClick = {this.onClickAdd.bind(this) } > Add </Button>
                {fields}
                {buttonDisplay}

            </Form>
        );
    }
}

EducationRender.defaultProps = {
    disabled : true,

    content:    [
        {
            examType: '',
            board   : '',
            percentage: ''
        }
    ]

}




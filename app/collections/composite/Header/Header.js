
    import React from 'react';
    import ReactDom from 'react-dom';
    import {MenuDefault} from '../../../components/menu';

    export default class Header extends React.Component {

        constructor(props) {
            super(props);
        }

            render() {
            return (
                <div className="ui inverted vertical masthead center aligned segment" style = {this.props.style}>
                    <MenuDefault navlink = {this.props.nav}/>
                </div>
            );
        }
    }
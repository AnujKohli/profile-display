
import React from 'react';
import ReactDom from 'react-dom';
import { default as ExperienceRender } from '../FormRender/ExperienceRender';
import {Grid, Column, Header} from 'semantic-react'


/*
 |--------------------------------------------------------------------------
 | Experience Display component
 |--------------------------------------------------------------------------
 |
 | Experience composite component which willl be rendered on Experience page
 |
 */

class ExperienceDisplay extends React.Component {
    constructor(props) {
        super(props);
        this.state={

        };
    }

    render() {
        return (
            <div>

                <Grid>
                    <Column width={6}></Column>
                </Grid>
                <Grid>
                    <Column width={2}></Column>
                    <Column width={6}>
                        <Header  color="black"> Experience Information </Header>
                    </Column>
                </Grid>
                <Grid>

                    <Column width={2}></Column>
                    <Column width={12}>
                    <ExperienceRender disabled = {this.props.disabled}/>
                    </Column>

                </Grid>

            </div>


        );
    }
}

ExperienceDisplay.defaultProps = {
    disabled : true,
    display: 'none'
}

ReactDom.render(<ExperienceDisplay />, document.getElementById('ExperienceDisplay'));


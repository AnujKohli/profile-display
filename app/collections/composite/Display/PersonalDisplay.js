
import React from 'react';
import ReactDom from 'react-dom';
import { default as PersonalInfo } from '../../Forms/Personal';
import {Grid, Column, Header} from 'semantic-react'

/*
 |--------------------------------------------------------------------------
 | Personal Display component
 |--------------------------------------------------------------------------
 |
 | Personal composite component which willl be rendered on Personal page
 |
 */
class PersonalDisplay extends React.Component {
    constructor(props) {
        super(props);
        this.state={

        };
    }


    render() {
        return (
            <div>

                <Grid>
                    <Column width={6}></Column>
                </Grid>
                <Grid>
                    <Column width={2}></Column>
                    <Column width={6}>
                        <Header  color="black"> Personal Information </Header>
                    </Column>
                </Grid>
                <Grid>
                    <Column width={2}></Column>
                    <Column width={12}>
                    <PersonalInfo disabled = {this.props.disabled} display={this.props.display} />

                    </Column>

                </Grid>

            </div>


        );
    }
}

PersonalDisplay.defaultProps = {
    disabled : true,
    display: 'none'
}

ReactDom.render(<PersonalDisplay />, document.getElementById('PersonalDisplay'));




import React from 'react';
import ReactDom from 'react-dom';
import { default as EducationRender } from '../FormRender/EducationRender';
import {Grid, Column, Header} from 'semantic-react'

/*
 |--------------------------------------------------------------------------
 | Education Display component
 |--------------------------------------------------------------------------
 |
 | Education composite component which will be rendered on Education page
 |
 */
class EducationDisplay extends React.Component {
    constructor(props) {
        super(props);
        this.state={

        };
    }

    render() {
        return (
            <div>


                <div>
                    <Header  color="black"> Education Information </Header>
                    <EducationRender disabled = {this.props.disabled}/>
                </div>

            </div>


        );
    }
}

EducationDisplay.defaultProps = {
    disabled : false,
    display: 'none'
}

ReactDom.render(<EducationDisplay />, document.getElementById('EducationDisplay'));


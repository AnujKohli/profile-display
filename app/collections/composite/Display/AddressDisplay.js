
import React from 'react';
import ReactDom from 'react-dom';
import { default as AddressRender } from '../FormRender/AddressRender';
import {Grid, Column, Header} from 'semantic-react'

/*
 |--------------------------------------------------------------------------
 | Address Display component
 |--------------------------------------------------------------------------
 |
 | Address composite component which will be rendered on Address page
 |
 */

class AddressDisplay extends React.Component {
    constructor(props) {
        super(props);
        this.state={

        };
    }

    render() {
        return (
            <div>

                <div>
                    <Header  color="black"> Address Information </Header>
                    <AddressRender disabled = {this.props.disabled}/>
                </div>

            </div>


        );
    }
}

AddressDisplay.defaultProps = {
    disabled : false,
    display: 'none'
}

ReactDom.render(<AddressInfo />, document.getElementById('AddressDisplay'));




import React from 'react';
import ReactDom from 'react-dom';

import {Field, Header, Grid, Column, Row, Steps,Step, label, Form, Button, Icon} from 'semantic-react';
import { default as PersonalInfo } from '../../Forms/Personal';
import { default as AddressRender } from '../FormRender/AddressRender';
import { default as ExperienceRender } from '../FormRender/ExperienceRender';
import { default as EducationRender } from '../FormRender/EducationRender';
import {Accordion, AccordionTitle, AccordionBody  } from 'semantic-react'


/*
 |--------------------------------------------------------------------------
 |Forms Fill Up component
 |--------------------------------------------------------------------------
 |
 | Forms Fill Up composite component which will be rendered on Forms Fill Up page
 | Displays all the composite forms created
 |
 */

    class FormsFillUp extends React.Component {
    constructor(props) {
        super(props);
        this.handleSave = this.handleSave.bind(this);
        this.state={
            values: [],
            activeContent : 1
        };
    }

        handleClick(index){
          //  e.target.value.active = true;
           this.setState ({ activeContent : index });
            console.log(index);
        }

        handleSave(e) {
            e.preventDefault();

            if (this.state.activeContent == 4) {

                //***********************************************************
                //here the profile can be saved to the database
                //***********************************************************

                window.location = '/ProfileDisplay';
            }
            this.setState ({ activeContent : this.state.activeContent + 1 });
        }


        render() {


            var stepContent;

            switch(this.state.activeContent) {
                case 1:
                { stepContent= <PersonalInfo disabled={this.props.disabled} fill = {this.props.fill} onClickSave = {this.handleSave}/>
                    break; }
                case 2:
                {
                    console.log("here 2");
                    stepContent= <AddressRender disabled={this.props.disabled} fill = {this.props.fill} onClickSave = {this.handleSave}/>
                    break; }
                case 3:
                {  stepContent= <ExperienceRender disabled={this.props.disabled} fill = {this.props.fill} onClickSave = {this.handleSave}/>
                    break;}
                case 4:
                {  stepContent= <EducationRender disabled={this.props.disabled} fill = {this.props.fill} onClickSave = {this.handleSave}/>
                    break;}
               /* case 5:
                { stepContent= <PersonalInfo disabled={this.props.disabled}/>
                    break;}*/
            }

            return (
                <div>


                    <Grid>
                        <Column width={6}></Column>
                    </Grid>
                    <Grid>
                        <Column width={6}></Column>
                        <Column width={6}>
                            <Header size="huge" color="black"> <Icon name = "user"/>FILL UP INFO</Header>
                        </Column>
                    </Grid>
                    <Grid>
                        <Column width={2}></Column>
                        <Column width={12}>


                            <Steps equalWidths>
                                <Step icon="truck" title="PersonalInfo" description="Fill in personal info" active = {this.state.activeContent == 1 ? true: false} onClick={this.handleClick.bind(this, 1)}/>
                                <Step icon="payment" title="Address Info" description="fill in address info"  active = {this.state.activeContent == 2 ? true: false} onClick={this.handleClick.bind(this, 2)}/>
                                <Step icon="payment" title="Experience Info" description="fill in experience info" active = {this.state.activeContent == 3 ? true: false} onClick={this.handleClick.bind(this, 3)}/>
                                <Step icon="payment" title=" Education Info" description="fill in education info"  active = {this.state.activeContent == 4 ? true: false} onClick={this.handleClick.bind(this, 4)} />
                            </Steps>

                            <div > {stepContent} </div>


                        </Column>

                    </Grid>

                </div>
            );
        }


        }

FormsFillUp.defaultProps = {
    disabled : false,
    fill: true

}

ReactDom.render(<FormsFillUp />, document.getElementById('FormsFillUp'));




import React from 'react';
import ReactDom from 'react-dom';

import {Field, Header, Grid, Column, Row, Steps,Step, label, Form, Button, Icon} from 'semantic-react';
import { default as PersonalInfo } from '../../Forms/Personal';
import { default as AddressRender } from '../FormRender/AddressRender';
import { default as ExperienceRender } from '../FormRender/ExperienceRender';
import { default as EducationRender } from '../FormRender/EducationRender';
import {Accordion, AccordionTitle, AccordionBody  } from 'semantic-react'


/*
 |--------------------------------------------------------------------------
 | Profile Display component
 |--------------------------------------------------------------------------
 |
 | Profile Display composite component which will be rendered on Profile Display page
 | Displays all the composite forms created
 |
 */

    class ProfileDisplay extends React.Component {


    constructor(props) {

        super(props);
        this.state={
            values: []
        };
    }
/*

        componentWillMount() {
            //!*****************************************************************************************
            //here data will be retreived from the database and then given as props to each form render
            //!*****************************************************************************************

            this.abc = 2;

            console.log(this.abc);

            this.mock = mockData;
        }
*/


        onAccordionChange(index) {
            // Remove key if presented, add key otherwise, preserve other keys
            this.setState({
                values: (this.state.values.indexOf(index) !== -1) ? this.state.values.filter(cur => cur !== index) : [...this.state.values, index]
            });
        }


        render() {

            console.log(this.abc);
            return (
                <div>


                    <Grid>
                        <Column width={6}></Column>
                    </Grid>
                    <Grid>
                        <Column width={6}></Column>
                        <Column width={6}>
                            <Header size="huge" color="black"> <Icon name = "user"/>USER PROFILE</Header>
                        </Column>
                    </Grid>
                    <Grid>
                        <Column width={2}></Column>
                        <Column width={12}>

                <Accordion styled fluid activeIndexes={this.state.values} onAccordionChange={this.onAccordionChange.bind(this)}>
                    <AccordionTitle index={1}>Personal Information</AccordionTitle>
                    <AccordionBody>
                        <PersonalInfo disabled = {this.props.disabled}/>
                    </AccordionBody>
                    <AccordionTitle index={2}>Address Information</AccordionTitle>
                    <AccordionBody>
                        <AddressRender disabled = {this.props.disabled}/>
                    </AccordionBody>
                    <AccordionTitle index={3}>Experience</AccordionTitle>
                    <AccordionBody>
                        <ExperienceRender disabled = {this.props.disabled} content = {this.props.mock.experienceInfo}/>
                    </AccordionBody>
                    <AccordionTitle index={4}>Education</AccordionTitle>
                    <AccordionBody>
                        <EducationRender disabled = {this.props.disabled} content = {this.props.mock.educationInfo}/>
                    </AccordionBody>

                </Accordion>

                        </Column>

                    </Grid>

                </div>
            );
        }


        }

ProfileDisplay.defaultProps = {
    disabled : true,

    mock: {

        educationInfo: [
            {
                examType: '10th',
                board: 'AISSCE',
                percentage: '93.4'

            },
            {
                examType: '12th',
                board:'AISSE',
                percentage:'93.6'
            }
        ],
        experienceInfo:[
            {
                company: 'company1',
                experience: '1'
            },
            {
                company: 'company2',
                experience: '2'
            },

            {
                company: 'company3',
                experience: '3'
            }
        ]

    }

}

ReactDom.render(<ProfileDisplay />, document.getElementById('ProfileDisplay'));


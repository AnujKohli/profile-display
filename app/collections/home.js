
import React from 'react';
import ReactDom from 'react-dom';


import {SideBarDefault} from './../components/sidebar';
import {Header} from './composite/Header';

// Content passed down the component tree. The are the contents of sidebars
var nav = ['Home', 'Carrier', 'Company'];

/**
 * Class to render SideBar to home.ejs
 */
class SideBarRender extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <SideBarDefault navlink = {nav}/>
        );
    }
}
ReactDom.render(<SideBarRender />, document.getElementById('sidebar'));


/**
 * Class to render menu to home.ejs
 */
/*
class MenuRender extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
           <MenuDefault navlink = {nav}/>
        );
    }
}
ReactDom.render(<MenuRender />, document.getElementById('menu'));*/


/**
 * Class to render SideBar to home.ejs
 */
class HeaderRender extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (

            <Header style= {this.props.style} nav= {this.props.nav} />
        );
    }
}

HeaderRender.defaultProps = {

    style:
    {
        backgroundImage: 'url(/images/hero-home-person.jpg)',
        backgroundSize : 'cover'
    },

    nav: ['Home', 'Carrier', 'Company']
}

ReactDom.render(<HeaderRender />, document.getElementById('header'));
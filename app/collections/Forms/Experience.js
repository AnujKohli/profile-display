
import React from 'react';
import ReactDom from 'react-dom';
import { InputRender } from '../../components/profileDisplay/InputRender';
import {Field, label, Form, Button, Icon} from 'semantic-react'

/*
 |--------------------------------------------------------------------------
 | Experience Form Component
 |--------------------------------------------------------------------------
 |
 | Experience Info component to be reused
 |
 */

export default class Experience extends React.Component {
    constructor(props) {
        super(props);

        console.log(this.props.disabled);
    }

    render() {
        return (
            <Field inline>

                        <label> Company's Name </label>
                        <InputRender type="text" placeholder="Company's Name" disabled ={this.props.disabled} value = {this.props.company} />

                        <label>No. of years worked</label>
                        <InputRender type="number" placeholder="No. of years worked" disabled ={this.props.disabled} value = {this.props.experience}/>

                       <Icon name="minus square" state = {this.props.iconState} onClick = {this.props.onClickMinus}/>

            </Field>

        );
    }
}

Experience.defaultProps = {
    disabled : true,

}


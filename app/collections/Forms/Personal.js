
import React from 'react';
import ReactDom from 'react-dom';
import { InputRender } from '../../components/profileDisplay/InputRender';
import { DatePicker } from '../../components/profileDisplay/DatePicker';
import {Field, label, Form, Button, SubHeader} from 'semantic-react'


/*
 |--------------------------------------------------------------------------
 | Personal Form Component
 |--------------------------------------------------------------------------
 |
 | Personal Info component to be reused
 |
 */

export default class PersonalInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            disabled: this.props.disabled,
            label   : "edit"

        };
        console.log(this.state.disabled)
    }

    onClick() {
        this.setState ( {
            disabled:!this.state.disabled,
            label   : this.state.disabled ? "edit":"save"
        })
        console.log(this.state.disabled)
    }
    render() {
        var buttonDisplay;
        if (this.props.fill)
        {
            buttonDisplay = <Button  onClick = {this.props.onClickSave}> Save & Continue </Button>

        }
        else
        {
            buttonDisplay = <Button  onClick = {this.onClick.bind(this)}> { (this.state.disabled)? 'edit': 'save'}  </Button>
        }

        return (
            <Form >
                <Field inline>

                    <SubHeader size="small" disabled color="violet">First Name</SubHeader>

                    <InputRender type="text" placeholder="First Name" disabled ={this.state.disabled}/>
                </Field>
                <Field inline>

                    <SubHeader size="small" disabled color="violet">Last Name</SubHeader>
                    <InputRender type="text" placeholder="Last Name" disabled ={this.state.disabled}/>
                </Field>
                <Field inline>

                    <SubHeader size="small" disabled color="violet">Father's Name</SubHeader>
                    <InputRender label="Father's Name" type="text" placeholder="Father's Name" disabled ={this.state.disabled}/>
                </Field>
                <Field inline>
                     <SubHeader size="small" disabled color="violet">Mother's Name </SubHeader>
                    <InputRender type="text" placeholder="Mother's Name" disabled ={this.state.disabled}/>
                </Field>
                <Field inline>
                    <SubHeader size="small" disabled color="violet">Date of Birth</SubHeader>
                    <DatePicker disabled ={this.state.disabled}/>
                </Field>

                {buttonDisplay}


            </Form>
        );
    }
}

PersonalInfo.defaultProps = {
    disabled : true,
    display: 'block'
}


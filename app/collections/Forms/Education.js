
import React from 'react';
import ReactDom from 'react-dom';
import { InputRender } from '../../components/profileDisplay/InputRender';
import { Select } from '../../components/profileDisplay/Select';
import {Field, label, Form, Button, Icon} from 'semantic-react'


/*
 |--------------------------------------------------------------------------
 | Education Form Component
 |--------------------------------------------------------------------------
 |
 | Education Info component to be reused
 |
 */

export default class Education extends React.Component {
    constructor(props) {
        super(props);

        console.log(this.props.disabled);
    }

    render() {
        return (
            <Field inline>

                        <label> Type of Exam</label>
                        <Select value = {this.props.examType}/>

                        <label>Education Board</label>
                        <InputRender type="text" placeholder="Board Name" disabled ={this.props.disabled} value = {this.props.board}/>

                        <label>Percentage Obtained</label>
                        <InputRender type="text" placeholder="%" disabled ={this.props.disabled} value = {this.props.percentage}/>
                       <Icon name="minus square" state = {this.props.iconState} onClick = {this.props.onClickMinus}/>

            </Field>

        );
    }
}

Education.defaultProps = {
    disabled : true,

}



import React from 'react';
import ReactDom from 'react-dom';
import { InputRender } from '../../components/profileDisplay/InputRender';
import { DatePicker } from '../../components/profileDisplay/DatePicker';
import {Field, label, Form, Button, SubHeader} from 'semantic-react'


/*
 |--------------------------------------------------------------------------
 | Address Form Component
 |--------------------------------------------------------------------------
 |
 | Address Info component to be reused
 |
 */

export default class Address extends React.Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <Form >
                <Field inline>
                    <SubHeader size="small" disabled color="violet">Street1</SubHeader>
                    <InputRender type="text" placeholder="Street1" disabled ={this.props.disabled}/>
                </Field>
                <Field inline>
                    <SubHeader size="small" disabled color="violet">Street2</SubHeader>
                    <InputRender type="text" placeholder="Street2" disabled ={this.props.disabled}/>
                </Field>
                <Field inline>
                    <SubHeader size="small" disabled color="violet">City</SubHeader>
                    <InputRender type="text" placeholder="City" disabled ={this.props.disabled}/>
                </Field>
                <Field inline>
                    <SubHeader size="small" disabled color="violet">Zip</SubHeader>
                    <InputRender type="text" placeholder="Zip" disabled ={this.props.disabled}/>
                </Field>
            </Form>
        );
    }
}

Address.defaultProps = {
    disabled : false
}




import React                        from 'react';
import ReactDom                     from 'react-dom';
import {Row}                        from 'semantic-react';
import {Grid}                       from 'semantic-react';
import {Column}                     from 'semantic-react';
import {Button, Image}              from 'semantic-ui-react';
import {Modal}                      from './../../components/Modal';
import {Tabs}                       from './../../components/Tabs';
import {Rating}                     from './../../components/Rating';
import {Select}                     from './../../components/profileDisplay/Select';
import {AccountMenu}                from '../../components/AccountMenu';
import {Comments}                   from '../../components/Comments';
import {AttorneySearch}             from '../../components/Search'
import {Items}                      from '../../components/Items';
import {InputRender}                from '../../components/profileDisplay/InputRender';
import {FileUpload}                 from '../../components/FileUpload';
import {BarMembership}              from '../../components/BarMembership';
import {CardDefault, CardAccept}    from '../../components/Card';

/*
 |--------------------------------------------------------------------------
 | Customizable Modal
 |--------------------------------------------------------------------------
 |
 | This will render custom Modal view in samples html
 |
 |
 */

class ModalRender extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false
        }
    }

    onClickHandler() {
        this.setState({ open: true});
    }

    render() {

        return (
                <div>
                    <Button onClick={this.onClickHandler.bind(this)}>Open Modal</Button>
                    <Row><Modal
                            open                =   {this.state.open}
                            modal_header        =   "Demo Modal (with custom props)"
                            image_size          =   "small"
                            image_src           =   "/images/-20-mb-format-psd-color-theme-blue-white-keywords-information-icon-2.jpg"
                            description_header  =   "Form Title"
                            description         =   "Your form or information goes here"
                            submit_btn_title    =   "OK"
                            close_btn_title     =   "Cancel"
                    /></Row>
                </div>
        );
    }
}

Modal.defaultProps = {
    modal_header:           "Demo Modal (with default props)",
    image_size:             "small",
    image_src:              "../images/information-icon-5.png",
    description_header:     "Form Title",
    description:            "Your form or information goes here",
    submit_btn_title:       "OK",
    close_btn_title:        "Cancel"
};

ReactDom.render(<ModalRender />, document.getElementById('sample-modal'));




/*
 |--------------------------------------------------------------------------
 | Customizable Tabs
 |--------------------------------------------------------------------------
 |
 | This will render custom Tabs view in samples html
 |
 |
 */

class TabsRender extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <Row><Tabs numberOfTtabs={2} menuTitles={menuTitles} tabsContent={tabsContent} /></Row>
            </div>
        );
    }
}


var menuTitles = [];
var tabsContent = [];

menuTitles.push("First");
menuTitles.push("Second");
menuTitles.push("Third");
tabsContent.push("Content for first tab");
tabsContent.push("Content for second tab");
tabsContent.push("Content for third tab");


ReactDom.render(<TabsRender />, document.getElementById('custom-tabs'));



/*
 |--------------------------------------------------------------------------
 | Ratings
 |--------------------------------------------------------------------------
 |
 | This will render ratings in samples html
 |
 |
 */

class RatingRender extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Rating />
        );
    }
}

ReactDom.render(<RatingRender />, document.getElementById('rating'));

/*
 |--------------------------------------------------------------------------
 | Select which allows additions
 |--------------------------------------------------------------------------
 |
 | This will render select with additions in samples html
 |
 |
 */

class SelectRender extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Select options={['dynamic a','dynamic b','dynamic c', 'dynamic d']} />
        );
    }
}

ReactDom.render(<SelectRender />, document.getElementById('select'));

/*
 |--------------------------------------------------------------------------
 | AccountMenu
 |--------------------------------------------------------------------------
 |
 | This will render AccountMenu in samples html
 |
 |
 */
class AccountMenuRender extends React.Component {
    constructor(props){
        super(props)
    }
    render(){

        const trigger = (<span><Image avatar src='https://avatars2.githubusercontent.com/u/6078720?v=3&s=200' />Krishna Singh</span>)
        const dropdownItem = [{text:'Account',icon:'user'},{text:'Settings',icon:'settings'},{text:'Sign Out',icon:'sign out'}]
        return(
            <AccountMenu dropdownItem={dropdownItem} trigger={trigger}/>
        );
    }
}
ReactDom.render(<AccountMenuRender/>, document.getElementById('account'));

/*
 |--------------------------------------------------------------------------
 | Comments
 |--------------------------------------------------------------------------
 |
 | This will render Comments in samples html
 |
 |
 */
class CommentsRender extends React.Component {
    constructor(props){
        super(props)
    }
    render(){
        const commentItem =[{avatarSrc:'http://semantic-ui.com/images/avatar/small/matt.jpg',author:'Matt',date:'Today at 5:42PM',text:'How artistic!'},
        {avatarSrc:'http://semantic-ui.com/images/avatar/small/elliot.jpg',author:'Elliot Fu',date:'Yesterday at 12:30AM',text:'This has been very useful for my research. Thanks as well!'},
        {avatarSrc:'http://semantic-ui.com/images/avatar/small/jenny.jpg',author:'Jenny Hess',date:'Just now',text:'Elliot you are always so right :)'},
        {avatarSrc:'http://semantic-ui.com/images/avatar/small/joe.jpg',author:'Joe Henderson',date:'5 days ago',text:' Dude, this is awesome. Thanks so much'}]
        return(
            <Comments commentItem={commentItem}/>
        );
    }
}
ReactDom.render(<CommentsRender/>, document.getElementById('comments'));

/*
 |--------------------------------------------------------------------------
 | Attorney Search
 |--------------------------------------------------------------------------
 |
 | This will render Attorney Search in samples html
 |
 |
 */
class AttorneySearchRender extends React.Component {
    constructor(props){
        super(props)
    }
    render(){
        return(
            <AttorneySearch/>
        );
    }
}
ReactDom.render(<AttorneySearchRender/>, document.getElementById('search'));

/*
 |--------------------------------------------------------------------------
 | Items ( somewhat like cards )
 |--------------------------------------------------------------------------
 |
 | This will render items with additions in samples html
 |
 |
 */

class ItemsRender extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Items content={this.props.content}/>
        )
    }
}

ItemsRender.defaultProps = {

    content: [

        {
            id: '1',
            img: 'http://semantic-ui.com/images/avatar2/large/matthew.png',
            header: 'Mathew',
            meta: 'Manager',
            description: "http://semantic-ui.com/images/wireframe/short-paragraph.png",
            text: 'extra description'
        },
        {
            id: '2',
            img: 'http://semantic-ui.com/images/avatar2/large/molly.png',
            header: 'Molly',
            meta: 'SSDE',
            description: "http://semantic-ui.com/images/wireframe/short-paragraph.png",
            text: 'extra description'
        }

    ]
};

ReactDom.render(<ItemsRender />, document.getElementById('items'));

/*
 |--------------------------------------------------------------------------
 | Card
 |--------------------------------------------------------------------------
 |
 | This will render cards in samples html
 |
 |
 */

class CardRender extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <CardDefault content={this.props.content} />
        );
    }
}

CardRender.defaultProps = {

    content:
        [
            {
                id: '1',
                img: 'http://semantic-ui.com/images/avatar2/large/matthew.png',
                header: 'Mathew',
                meta: 'Joined in 2015',
                description: 'Matthew is a musician living in Nashville',
                extra: '22 Friends'
            },

            {
                id: '2',
                img: 'http://semantic-ui.com/images/avatar2/large/molly.png',
                header: 'Molly',
                meta: 'Joined in 2014',
                description: 'Molly is a review attorney living in Houston',
                extra: '54 Friends'
            }
        ]
}
ReactDom.render(<CardRender />, document.getElementById('card'));

/*
 |--------------------------------------------------------------------------
 | Card
 |--------------------------------------------------------------------------
 |
 | This will render cards in samples html
 |
 |
 */

class CardAcceptRender extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <CardAccept content={this.props.content} />
        );
    }
}

CardAcceptRender.defaultProps = {

    content:
        [
            {
                id: '1',
                img: 'http://semantic-ui.com/images/avatar2/large/matthew.png',
                header: 'Mathew',
                meta: 'Joined in 2015',
                description: 'Matthew is a musician living in Nashville',
                extra: '22 Friends'
            },

            {
                id: '2',
                img: 'http://semantic-ui.com/images/avatar2/large/molly.png',
                header: 'Molly',
                meta: 'Joined in 2014',
                description: 'Molly is a senior developer living in Houston',
                extra: '54 Friends'
            }
        ]
}
ReactDom.render(<CardAcceptRender />, document.getElementById('cardAccept'));


class InputRenderer extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <InputRender name="myNumber" type="number"/>
            </div>
        );
    }
}


ReactDom.render(<InputRenderer />, document.getElementById('input'));

class FileUploadRender extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <FileUpload />
            </div>
        );
    }
}

ReactDom.render(<FileUploadRender />, document.getElementById('file-upload'));


class BarMembershipRender extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <BarMembership />
            </div>
        );
    }
}

ReactDom.render(<BarMembershipRender />, document.getElementById('bar-membership'));
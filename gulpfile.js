// gulpfile.js 
var gulp         = require('gulp');
var server       = require('gulp-express');
var webpack      = require('webpack');
var gutil        = require('gulp-util');
var spawn        = require('child_process').spawn;
var webpackconf  = require('./webpack.config.js');
 

gulp.task('server', function (done) {
     var debug = true;

    // Run webpack
    webpack(webpackconf).run(onBuild(done, server));
    // server.notify(event);

    // Start the server at the beginning of the task 
    server.run(['server.js']);

    const webpack_watch = spawn('webpack', ['--watch']);

    webpack_watch.stdout.on('data', (data) => {
        //console.log(`stdout: ${data}`);
        say('Webpack: output ', "Bundle Prepared");

        // notifying server
        server.notify;

        // notified to server
        say('Server : Render', "Notified Browser");
    });

 
    // Restart the server when file changes 
    gulp.watch(['app/**/*.html'], server.notify);
    gulp.watch(['app/**/*.ejs'], server.notify);
    gulp.watch(['app/**/*.js'], server.notify);
    
    //gulp.watch(['app/styles/**/*.scss'], ['styles:scss']);
    //gulp.watch(['{.tmp,app}/styles/**/*.css'], ['styles:css', server.notify]); 
    //Event object won't pass down to gulp.watch's callback if there's more than one of them. 
    //So the correct way to use server.notify is as following: 
    gulp.watch(['app/**/*.html','app/**/*.ejs','app/**/*.js'], function(event){
        // gulp.run('styles:css');
         //webpack(webpackconf).run(onBuild(done));

         //pipe support is added for server.notify since v0.1.5,
         //see https://github.com/gimm/gulp-express#servernotifyevent
         //say('Webpack: ', "Please Wait preparing bundle");
         //webpackWatch(server, event);
     });
 
    // gulp.watch(['public/images/**/*'], server.notify);
    // TODO - ADD TASK JSHINT gulp.watch(['app/**/*.js'], ['jshint']);
    gulp.watch(['app.js', 'app/routes/**/*.js'], [server.run]);
});


// Webpack logger on build
function onBuild(done, server) {
    return function(err, stats) {

        if (err) {
            gutil.log('Error', err);
            if (done) {
                done();
            }
        } else {
            server.notify();
            Object.keys(stats.compilation.assets).forEach(function(key) {
                gutil.log('Webpack: output ', gutil.colors.green(key));
            });
            gutil.log('Webpack: ', gutil.colors.blue('finished ', stats.compilation.name));

            if (done) {
                done();
            }
        }
    }
}

// Webpack watch -spawn - Not being used right now
function webpackWatch(server, event) {
    // Start Webpack watch
    const webpack_watch = spawn('webpack', ['--watch']);

    webpack_watch.stdout.on('data', (data) => {
        //console.log(`stdout: ${data}`);
        say('Webpack: output ', "Bundle Prepared");

        // notifying server
        server.notify(event);
    });

    webpack_watch.stderr.on('data', (data) => {
        console.log(`stderr: ${data}`);
    });

    webpack_watch.on('close', (code) => {
        console.log(`child process exited with code ${code}`);
    });
}


// Logger msg
function say(out, msg) {
    gutil.log(out, gutil.colors.green(msg));
}